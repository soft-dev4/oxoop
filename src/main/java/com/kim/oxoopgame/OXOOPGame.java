package com.kim.oxoopgame;

/**
 *
 * @author ASUS
 */
public class OXOOPGame {

    public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if (game.isFinish()) {
                game.showTable();
                game.showResult();
                game.showWin();
                game.newBoard();

            }

        }
    }
}