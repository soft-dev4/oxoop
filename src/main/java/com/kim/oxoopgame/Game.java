package com.kim.oxoopgame;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class Game {

    private Player o;
    private Player x;
    private int row;
    private int col;
    private Board board;
    Scanner sc = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }

    public void newBoard() {
        this.board = new Board(o, x); //Create new Baord
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println("Turn" + " " + player.getSymbol());
    }

    public void showTable() {
        char[][] table = this.board.getTable();
        for (char[] table1 : table) {
            for (int c = 0; c < table1.length; c++) {
                System.out.print(table1[c]);
            }
            System.out.println("");
        }
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row,col:");
            row = sc.nextInt();
            col = sc.nextInt();

            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }

    public boolean isFinish() {
        return board.isDraw() || board.isWin();
    }

    public void showWin() {
        System.out.println(o);
        System.out.println(x);
    }

    public void showResult() {
        if (board.isDraw()) {
            System.out.println("Draw!!!");
        } else if (board.isWin()) {
            System.out.println(board.getCurrentPlayer().getSymbol() + "Win");

        }

    }

}
